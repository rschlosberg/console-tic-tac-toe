# The print_board function takes a list of
# nine values and prints them in a "pretty"
# 3x3 board

def print_board(board):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in board:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

current_board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for turn in range(9):
    print_board(current_board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response)
    current_board[space_number-1] = current_player

    #checking all rows for winners

    if current_board[0] == current_board[1] and current_board[1] == current_board[2]:
        print_board(current_board)
        print(current_board[0], "has won")
        exit()

    if current_board[3] == current_board[4] and current_board[4] == current_board[5]:
        print_board(current_board)
        print(current_board[0], "has won")
        exit()

    if current_board[6] == current_board[7] and current_board[7] == current_board[8]:
        print_board(current_board)
        print(current_board[0], "has won")
        exit()

    #checking all columns for winners

    if current_board[0] == current_board[3] and current_board[3] == current_board[6]:
        print_board(current_board)
        print(current_board[0], "has won")
        exit()

    if current_board[1] == current_board[4] and current_board[4] == current_board[7]:
        print_board(current_board)
        print(current_board[0], "has won")
        exit()

    if current_board[2] == current_board[5] and current_board[5] == current_board[8]:
        print_board(current_board)
        print(current_board[0], "has won")
        exit()
    #checking all 2 diagnols for winners
    if current_board[0] == current_board[4] and current_board[4] == current_board[8]:
        print_board(current_board)
        print(current_board[0], "has won")
        exit()
    if current_board[2] == current_board[4] and current_board[4] == current_board[6]:
        print_board(current_board)
        print(current_board[0], "has won")
        exit()

    ## tie game 
    if turn == 8:
        print_board(current_board)
        print("Tie game!")
        exit()

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"





    #Functional requirements
    #players must choose empty space
    #players must choose valid number
    #game must recognize winner
    #game must recognize tie

    #Get it to print the unchanged board 9 times
    #get it to ask for the correct players turn 9 times

    #get input from the players
    #update board

    #start figuringi out winners

    #finish figuring out winner
    #last checks to declare winner/tie

